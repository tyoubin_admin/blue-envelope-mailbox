import { ECommunicationStatus, ICommunicationInfo } from "@/models/communication";
import { University } from "@/pages/signup/components/school";

export interface IUniversityRes {
    [key: string]: University;
}

export interface IUniversityReq {
    page: number;
    per_page: number;
    keyword?: string;
}

export interface ICommunicationReq {
    page: number;
    per_page: number;
    status?: ECommunicationStatus;
}

export interface ICommunicationInfoRes {
    comm_list: ICommunicationInfo[]
}

export interface ICommunicationLetterReq {
    page: number;
    per_page: number;
    cmmn_id: number; //通信id
}

export interface IUploadReq {
    type: string;
    title: string;
}

export interface IUploadRes {
    accessid: string;
    callback: string;
    callback_var: string;
    dir: string;
    host: string;
    policy: string;
    signature: string;
    oss_meta: string;
}

export interface IUploadOssReq {
    file: string;
}

export interface IUploadOssRes {
    file_id: number
    file_url: string;
    status: 1;
}

export interface IWriteLetterReq {
    cmmn_id: number; //通信id
    letter_ids: string; //上传的信件图片id，多张以逗号','分隔
    remark?: string; //打卡记录
}

export interface IWriteLetterRes {
    event_id: string; //写信id
    charity_hours: string //获取的志愿时
}

export interface IApplyReq {
    [key: string]: string;
}

export interface IApply2Resp {
    info: 'ok' | 'fail',
    score: number;
    pass_score: number;
    all_score: number;
}