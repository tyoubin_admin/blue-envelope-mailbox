import { observer } from 'mobx-react'

import applyModel from './apply';
import communicationModel from './communication'
import globalModel from './global'
import userModel from './user'

export * from './user';

/**
 * 持久化数据容器
 * @param Component 组件
 * @returns
 */
export const WrapModel = function (Component: any) {
  return observer(Component);
};

const models = {
  /** 全局数据 */
  global: globalModel,
  /** 用户数据 */
  user: userModel,
  /** 通信数据 */
  communication: communicationModel,
  /** 考核数据 */
  apply: applyModel,
};

export default models;
