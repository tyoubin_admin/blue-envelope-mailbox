import { action, computed, observable } from 'mobx'

export class GlobalModel {
  /** 通用信息 */
  @observable private _generalInfo: any;
  /** 系统信息 */
  @observable private _systemInfo: any;

  @action
  setGeneralInfo(info: any) {
    this._generalInfo = info;
  }
  @computed
  get generalInfo() {
    return this._generalInfo;
  }

  @action
  setSystemInfo(info: any) {
    this._systemInfo = info;
  }
  @computed
  get systemInfo() {
    return this._systemInfo;
  }
}

const globalModel = new GlobalModel();

export default globalModel;
