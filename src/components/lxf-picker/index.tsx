import './index.scss'

import classNames from 'classnames'
import { Picker, View, Image } from '@tarojs/components'
import { PickerDateProps, PickerMultiSelectorProps, PickerRegionProps, PickerSelectorProps, PickerStandardProps, PickerTimeProps } from '@tarojs/components/types/Picker';
import { ComponentType } from 'react';

import IconPickerArrow from './assest/picker-arrow.svg'

const baseCls = 'lxf-picker';

type ILxfPickerProps = ComponentType<PickerStandardProps | PickerSelectorProps | PickerMultiSelectorProps | PickerTimeProps | PickerDateProps | PickerRegionProps> & {
    children: React.ReactNode;
    className: string;
    placeholder: string;
}

const LxfPicker = (props: ILxfPickerProps) => {
    const { children, className, placeholder, ...rest } = props;
    return (
        <Picker className={classNames(baseCls, className)} {...rest}>
            <View className={`${baseCls}-value`}>
                {children || placeholder}
            </View>
            <Image className={`${baseCls}-icon`} src={IconPickerArrow} />
        </Picker>
    )
}

export default LxfPicker;