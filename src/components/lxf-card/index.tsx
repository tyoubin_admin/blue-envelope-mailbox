import './index.scss'

import classNames from 'classnames'
import React from 'react'
import { Image, Text, View } from '@tarojs/components'

const baseCls = 'lxf-card';

interface ILxfButtonProps {
    title: string;
    subTitle?: string;
    children?: React.ReactNode;
    className?: string;
}

const LxfCard = (props: ILxfButtonProps) => {
    const { title, subTitle, className, children } = props;
    return (
        <View className={classNames(baseCls, className)} >
            <View className={classNames(`${baseCls}-title`, 'lxf_h3_text')}>
                <Text>{title}</Text>
                {subTitle && <Text className={'lxf_info_text_small'}>{subTitle}</Text>}
            </View>
            {children}
        </View>
    )
}

export default LxfCard;