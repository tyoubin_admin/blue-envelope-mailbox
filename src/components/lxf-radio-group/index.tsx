import './index.scss'

import classNames from 'classnames'
import { RadioGroup, RadioGroupProps, View } from '@tarojs/components'

import LxfRadio from '../lxf-radio'

const baseCls = 'lxf-radio-group';
interface ILxfRadioGroupOption<T> {
    label: string;
    value: T;
}

interface ILxfRadioGroupProps<T> extends RadioGroupProps {
    options?: ILxfRadioGroupOption<T>[];
    className?: string;
}

const LxfRadioGroup = (props: ILxfRadioGroupProps<any>) => {
    const { options = [], className, ...rest } = props;
    return (
        <RadioGroup className={classNames(baseCls, className)} {...rest}>{
            options.map(opt =>
                <LxfRadio value={opt.value} label={opt.label} />
            )}
        </RadioGroup>
    )
}

export default LxfRadioGroup;