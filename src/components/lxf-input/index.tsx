import './index.scss'

import classNames from 'classnames'
import { Input, InputProps } from '@tarojs/components'

const baseCls = 'lxf-input';

interface ILxfInputProps extends InputProps {
    className?: string;
    [key: string]: any;
}

const LxfInput = (props: ILxfInputProps) => {
    const { className, ...rest } = props;
    return (
        <Input className={classNames(baseCls, className)} placeholderClass={`${baseCls}-placeholder`} {...rest} />
    )
}

export default LxfInput;