import './index.scss'

import classNames from 'classnames'
import React from 'react'
import { Button, ButtonProps, Image, Text, View } from '@tarojs/components'

const baseCls = 'lxf-button';

interface ILxfButtonProps extends ButtonProps {
    title: string;
    icon?: any;
    disabled?: boolean;
    className?: string;
    onClick?: () => void;
}

const LxfButton = (props: ILxfButtonProps) => {
    const { title, icon, disabled = false, className, onClick, ...rest } = props;
    return (
        <Button className={classNames(baseCls, className, { disabled })} disabled={disabled} onClick={onClick} {...rest}>
            {icon && <Image className={`${baseCls}-icon`} src={icon} />}
            <Text className={`${baseCls}-text`}> {title} </Text>
        </Button>
    )
}

export default LxfButton;