import { useCallback, useMemo, useState } from 'react';
import classNames from 'classnames';
import { Image, View } from '@tarojs/components';
import closeIcon from '@/assets/icons/close.svg'


import { IFloatBaseProps, useFloatContentRenderable } from '../lxf-float-wrapper'

import './index.scss';

export type IModalProps = IFloatBaseProps & {};

const baseCls = 'common-modal';

const LxfModal: React.FunctionComponent<IModalProps> = props => {
  const {
    theme = 'light',
    style,
    visible,
    title,
    hideTitle,
    onOk,
    okText,
    showOkBtn,
    onCancel,
    cancelText,
    showCancelBtn,
    closable = false,
    maskClosable,
    footer,
    children,
    forceRender,
    className,
  } = props;

  const renderable = useFloatContentRenderable(visible ?? false, forceRender);

  return (
    <>
      <View
        className={classNames(baseCls, className, {
          [`${baseCls}--${theme}`]: true,
          show: visible,
        })}
      >
        <View className={`${baseCls}-content`} style={style}>
          {!hideTitle && !!title && (
            <View className={`${baseCls}-title`}>{title}</View>
          )}
          {renderable && children}
        </View>

        {footer !== undefined
          ? footer
          : (showOkBtn || showCancelBtn) && (
            <View className={`${baseCls}-bottom`}>
              {showCancelBtn && (
                <View className={`${baseCls}-btn`} onClick={onCancel}>
                  {cancelText || '取消'}
                </View>
              )}
              {showOkBtn && (
                <View
                  className={classNames(`${baseCls}-btn`, {
                    primary: true,
                  })}
                  onClick={onOk}
                >
                  {okText || '确认'}
                </View>
              )}
            </View>
          )}

        {closable && (
          <View
            className={`${baseCls}-close`}
            onClick={() => onCancel?.('close')}
          >
            <Image className={`${baseCls}-close-icon`} src={closeIcon} />
          </View>
        )}
      </View>
      <View
        // @ts-ignore
        catchMove
        className={classNames(`${baseCls}-mask`, {
          show: visible,
        })}
        onClick={() => maskClosable && onCancel?.('close')}
      />
    </>
  );
};

export default LxfModal;

export function useModal() {
  const [modalProps, setModalProps] = useState<IModalProps>();
  const [visible, setVisible] = useState(false);

  const modal = useMemo(() => {
    const handleOk = () => {
      setVisible(false);
      modalProps?.onOk?.();
    };

    const handleCancel = () => {
      setVisible(false);
      modalProps?.onCancel?.();
    };

    return (
      <LxfModal
        {...modalProps}
        visible={visible}
        onOk={handleOk}
        onCancel={handleCancel}
      />
    );
  }, [visible, modalProps]);

  const openFn = useCallback((openProps: IModalProps) => {
    setModalProps(openProps);
    setVisible(true);
  }, []);

  return [modal, openFn] as const;
}
