import './index.scss'

import { FunctionComponent } from 'react'
import { View } from '@tarojs/components'

import { LxfFloatWrapper } from '@/components'

const baseCls = 'lxf-confirm';

interface ILxfConfirmProps {
  content: string;
}

const LxfConfirm: FunctionComponent<ILxfConfirmProps> = (props) => {
  const { content } = props;
  return (
    <View className={baseCls}>{content}</View>
  );
};

export default LxfConfirm;

export const LxfConfirmModal = LxfFloatWrapper.withModal<ILxfConfirmProps>({})(LxfConfirm);
