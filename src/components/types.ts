import { CSSProperties, ReactNode } from 'react';

export type IBaseComponentProps = {
    theme?: 'light' | 'dark';
    className?: string;
    style?: CSSProperties;
    children?: ReactNode;
  };