/**
 * 创建自定义错误
 * @param message 错误信息
 * @param payload 携带内容
 * @returns
 */
export function createCustomError<T = any>(message: string, payload?: T) {
    console.error(message, payload);

    const error = new Error(message) as Error & { payload?: T };
    error.payload = payload;

    return error;
}