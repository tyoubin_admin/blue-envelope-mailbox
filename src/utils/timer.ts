/**
 * 睡眠
 * @param time 睡眠时长，单位毫秒
 * @returns
 */
export function sleep(time: number) {
  return new Promise(resolve => setTimeout(resolve, time));
}

/**
 * 等待
 * @param time 等待时长
 * @param fn 触发函数
 */
export function wait(time: number, fn: Function) {
  const timer = setTimeout(fn, time);

  return () => clearTimeout(timer);
}
