import { EApplyStatus, EFavourKey, EFavourValue, EGender, EIdType } from './volunteer'

export const applyStatus2text = (val: EApplyStatus) => {
  const map = {
    [EApplyStatus.CAMPUS]: "在职大使",
    [EApplyStatus.CORPORATE]: "在校大使"
  };

  return map[val];
};

export const gender2text = (val: EGender) => {
  const map = {
    [EGender.BOY]: "男",
    [EGender.GIRL]: "女"
  };

  return map[val];
};

export const idType2text = (val: EIdType) => {
  const map = {
    [EIdType.JUNIOR_COLLEGE_STUDENT]: "在校专科生",
    [EIdType.UNDERGRADUATE_STUDENTS]: "在校本科生",
    [EIdType.MASTERS]: '在校硕士研究生',
    [EIdType.DOCTORS]: '在校博士研究生',
    [EIdType.HIGH_SCHOOL_STUDENTS]: '在校高中生'
  };

  return map[val];
};

export const favourValue2text = (val: EFavourValue) => {
  const map = {
    [EFavourValue.MAJOR]: "主科（语文、数学、英语）",
    [EFavourValue.LIBERAL_ARTS]: '文科（历史、地理、政治）',
    [EFavourValue.SCIENCE]: '理科（生物、物理、化学）',
    [EFavourValue.FRIEND]: '朋友关系',
    [EFavourValue.FAMILY]: '家庭关系',
    [EFavourValue.SELF]: '自我相处（心情/情绪/心理）',
    [EFavourValue.SOCIAL]: '社交类（老师、企业管理、营销管理人员等喜欢与人交往、具有协调合作才能的职业类型）',
    [EFavourValue.CREATIVE]: '创作类（设计师、作家、广告人、导演等有创造力，渴望表现自己的个性的职业类型） ',
    [EFavourValue.TECH]: '技术研究类（程序员、工程师、医生、厨师等动手能力强，喜欢独立做事的职业类型）',
    [EFavourValue.SPORTS]: '运动类',
    [EFavourValue.ART]: '艺术类',
    [EFavourValue.LEISURE]: '休闲类'
  };

  return map[val];
};

export const favourKey2text = (val: EFavourKey) => {
  const map = {
    [EFavourKey.LEARNING]: '学习方面',
    [EFavourKey.RELATIONSHIP]: '关系处理方面',
    [EFavourKey.CAREER]: '职业方面',
    [EFavourKey.LIFE]: '生活方面'
  };

  return map[val];
};

export const favourOptions = {
  [EFavourKey.LEARNING]: [EFavourValue.MAJOR, EFavourValue.LIBERAL_ARTS, EFavourValue.SCIENCE],
  [EFavourKey.RELATIONSHIP]: [EFavourValue.FRIEND, EFavourValue.FAMILY, EFavourValue.SELF],
  [EFavourKey.CAREER]: [EFavourValue.SOCIAL, EFavourValue.CREATIVE, EFavourValue.TECH],
  [EFavourKey.LIFE]: [EFavourValue.SPORTS, EFavourValue.ART, EFavourValue.LEISURE]
}
