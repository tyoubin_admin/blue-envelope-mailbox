import models from '@/models'

export function useGlobal() {
  return [models.global] as const;
}

export function useSystemInfo() {
  const [global] = useGlobal();
  return [global.systemInfo] as const;
}