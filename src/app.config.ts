export default {
  pages: [
    'pages/index/index',
    'pages/my/index',
    'pages/mailbox/index',
    'pages/welcome/index',
    'pages/in-application/index',
    'pages/signup/step1/index',
    'pages/signup/step2/exam/index',
    'pages/signup/step2/video/index',
    'pages/signup/step3/index',
    'pages/my/mobile-modify/index',
    'pages/mailbox/write/index',
    'pages/my/person/index',
    'pages/certificate/index',
    'pages/login/index',
    'pages/disuse/index'
  ],
  tabBar: {
    list: [
      {
        pagePath: 'pages/mailbox/index',
        text: '信箱',
        iconPath: './assets/images/footer/mailbox.png',
        selectedIconPath: './assets/images/footer/mailbox-active.png',
      },
      {
        pagePath: 'pages/my/index',
        text: '我的',
        iconPath: './assets/images/footer/my.png',
        selectedIconPath: './assets/images/footer/my-active.png',
      },
    ],
  },
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'light'
  }
}
