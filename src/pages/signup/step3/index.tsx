import './index.scss'

import Taro from '@tarojs/taro'
import classNames from 'classnames'
import { useEffect } from 'react'
import { Form, View } from '@tarojs/components'

import { useApply, withUser } from '@/hooks'
import { goToPage } from '@/utils'
import { LxfButton, LxfCard } from '@/components'
import api from '@/api'
import { IApplyInfo } from '@/models/apply'
import { form2Comp } from '../util'
const baseCls = 'signup-page-step3';

const SignUp = () => {
    const [apply] = useApply();
    const { step3ApplyInfo = {} } = apply ?? {};
    const { form_item_list = [], question_list = [], question_description } = step3ApplyInfo as IApplyInfo;

    const handleSignUp = (e: any) => {
        Taro.showModal({
            title: '确定提交问答文字的内容？',
            confirmText: '提交',
            success: async (res) => {
                if (res.confirm) {
                    const info = e.target.value;
                    const params = { ...info };
                    const res = await api.lxf.postVolunteerApplyStep3Answer(params);
                    if (res.info === "ok") {
                        Taro.showModal({
                            title: '恭喜你通过考试',
                            showCancel: false,
                            confirmText: '我知道了',
                            content: res.info,
                            success: async (res) => {
                                if (res.confirm) {
                                    goToPage('/pages/in-application/index');
                                }
                            }
                        })
                    } else if (res.info === "fail") {
                        Taro.showModal({
                            title: '分数不足，未能通过考试',
                            confirmText: '继续答题',
                            showCancel: false,
                            content: res.info,
                        })
                    }
                }
            }
        })
    }

    useEffect(() => {
        if (!apply.step3ApplyInfo) {
            apply.getStep3ApplyInfo();
        }
    }, [])

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={`${baseCls}-info`}>
                    <View className={classNames('lxf_info_text')}>
                        {question_description}
                    </View>
                    <Form onSubmit={handleSignUp} className={`${baseCls}-info`}>
                        {[...question_list, ...form_item_list]?.map((item) => {
                            if (!item) return null;
                            const { title, required, type, name, options: _options } = item;
                            const options = Array.isArray(_options) ?
                                _options.map(opt => ({ label: opt, value: opt }))
                                : typeof _options === "object" ?
                                    Object.values(_options)?.map(opt => ({ label: opt, value: opt }))
                                    : []
                            const Comp = form2Comp(type);
                            return type === "description" ?
                                <View className={classNames('lxf_info_text')}>
                                    {title}
                                </View>
                                : <LxfCard title={`${title}${required ? '*' : ''}`} >
                                    {Comp ?
                                        <Comp
                                            mode={type}
                                            name={name}
                                            options={options}
                                            placeholder={`请输入${title}`}
                                        />
                                        : null}
                                </LxfCard>
                        })}

                        <View className={`${baseCls}-submit-btn`}>
                            <LxfButton title='提交' formType='submit' />
                        </View>
                    </Form>
                </View>
            </View>
        </View>
    )
}
export default withUser({
    auto: true,
    inject: true
})(SignUp)
