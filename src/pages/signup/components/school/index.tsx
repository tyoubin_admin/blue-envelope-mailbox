import './index.scss'
import { View } from '@tarojs/components'
import { LxfPicker } from '@/components'
import { useEffect, useState } from 'react';
import api from '@/api';

const baseCls = 'info-city';

export interface University {
    id: number;
    name: string;
}

const UniversityComp = (props) => {
    const [universityList, setUniversityList] = useState<University[]>([]);
    const [university, setUniversity] = useState<string>();
    const fetchUniversityList = async (keyword?: string) => {
        const res = await api.lxf.getUniversity({ keyword, page: 1, per_page: 20 });
        console.log('res------', Object.values(res))
        setUniversityList(Object.values(res).filter(item => item.id));
    }

    useEffect(() => {
        fetchUniversityList();
    }, []);

    return (
        <View className={`${baseCls}`}>
            <LxfPicker {...props} mode='selector' range={universityList} rangeKey={"name"} onChange={(e) => {
                console.log('选项列表', e.target.value)
                setUniversity(universityList[e.target.value].name)
            }} {...props}>
                {university}
                {/* <LxfInput placeholder={"输入学校名称"} /> */}
            </LxfPicker>
        </View>
    )
}
export default UniversityComp;
