import './index.scss'
import { View } from '@tarojs/components'
import { LxfPicker } from '@/components'
import { useState } from 'react';

const baseCls = 'info-city';

const provinceList = ["北京", "天津", '广东'];
const cityList = ["北京", "天津", '广州'];

const CityComp = (props) => {
    const { province, city, setCity, setProvince } = props;
    const [region, setRegion] = useState<string[]>([]);

    return (
        <LxfPicker {...props} mode='region' onChange={(e) => setRegion(e.target.value)} placeholder={"选择省"} >
            {region.join(',')}
        </LxfPicker>
    )

    // return (
    //     <View className={`${baseCls}`}>
    //         <LxfPicker name='province' mode='selector' range={provinceList} onChange={(e) => setProvince(provinceList[e.target.value])} placeholder={"选择省"}>
    //             {province}
    //         </LxfPicker>
    //         <LxfPicker name='city' mode='selector' range={cityList} onChange={(e) => setCity(cityList[e.target.value])} placeholder={"选择市"}>
    //             {city}
    //         </LxfPicker>
    //     </View>
    // )
}
export default CityComp;
