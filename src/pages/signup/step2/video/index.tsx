import './index.scss'

import classNames from 'classnames'
import { useEffect } from 'react'
import { View } from '@tarojs/components'

import { useApply, withUser } from '@/hooks'
import { goToPage } from '@/utils'
import { LxfButton } from '@/components'
import { IApplyInfo } from '@/models/apply'
import LxfVideo from '@/components/lxf-video'
import Taro from '@tarojs/taro'
import { STORAGE_GENERAL_INFO_KEY } from '@/constants'

const baseCls = 'watch-video';

const VideoPage = () => {
    const [apply] = useApply();
    const { step2ApplyInfo = {} } = apply ?? {};
    const { video_list = [] } = step2ApplyInfo as IApplyInfo;
    useEffect(() => {
        if (!apply.step2ApplyInfo) {
            apply.getStep2ApplyInfo();
        }
        const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
        Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {...data, videoVisited: true });
    }, [])
    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={"lxf_h1_text"}>二、观看书信培训视频</View>
                <View className={`${baseCls}-info`}>
                    <View className={classNames('lxf_info_text')}> 书信培训视频将带你认识通信规范、收发信运作等与你密切相关的信息，是否观看视频将会作为综合评定你能否成为通信大使的考核项之一，请务必观看。</View>
                    <View className={"lxf_h2_text"}>课程视频</View>
                    {video_list.map(item => {
                        if (!item) return null;
                        return <LxfVideo src={item.url} poster={item.cover} />
                    })}
                </View>
                <View className={`${baseCls}-submit-btn`}>
                    <LxfButton title='学习完毕，去考核' onClick={() => goToPage('/pages/signup/step2/exam/index')} />
                </View>
            </View>
        </View>
    )
}
export default withUser({
    auto: true,
    inject: true
})(VideoPage)
