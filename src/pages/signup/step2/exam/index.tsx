import './index.scss'

import classNames from 'classnames'
import { useEffect, useRef, useState } from 'react'
import { Form, RichText, View } from '@tarojs/components'

import { useApply, withUser } from '@/hooks'
import { goToPage, message } from '@/utils'
import { LxfButton, LxfCard } from '@/components'
import api from '@/api'
import { IApplyInfo } from '@/models/apply'
import { form2Comp } from '../../util'
import { LxfConfirmModal } from '@/components/lxf-confirm-model'
import { IFloatWrapperRef } from '@/components/lxf-float-wrapper'
import { IApply2Resp } from '@/service/type'

const baseCls = 'signup-page';

const SignUp = () => {
    const signRef = useRef<IFloatWrapperRef>();
    const passRef = useRef<IFloatWrapperRef>();
    const cannotPassRef = useRef<IFloatWrapperRef>();
    const [apply] = useApply();
    const { step2ApplyInfo = {} } = apply ?? {};
    const { form_item_list = [], question_list = [], question_description } = step2ApplyInfo as IApplyInfo;
    const [formValues, setFormValues] = useState<any>();
    const [step2Res, setStep2Res] = useState<IApply2Resp>();

    const handleConfirm = async () => {
        const info = formValues;
        const params = { ...info };
        const res = await api.lxf.postVolunteerApplyStep2Answer(params);
        if (res && res.info === "ok") {
            setStep2Res(res);
            if (res.score >= res.pass_score) {
                passRef.current?.open();
            } else {
                cannotPassRef.current?.open();
            }
        } else {
            message.error(res.info)
        }
    }

    const handleSignUp = (e: any) => {
        setFormValues(e.target.value);
        signRef.current?.open();
    }

    useEffect(() => {
        if (!apply.step2ApplyInfo) {
            apply.getStep2ApplyInfo();
        }
    }, [])

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={"lxf_h1_text"}>三、理论考试</View>
                <View className={`${baseCls}-info`}>
                    <RichText className={classNames('lxf_info_text')} nodes={question_description} />
                    <Form onSubmit={handleSignUp} className={`${baseCls}-info`}>
                        {[...question_list, ...form_item_list]?.map((item) => {
                            if (!item) return null;
                            const { title, required, type, name, options: _options } = item;
                            const options = Array.isArray(_options) ?
                                _options.map(opt => ({ label: opt, value: opt }))
                                : typeof _options === "object" ?
                                    Object.values(_options)?.map(opt => ({ label: opt, value: opt }))
                                    : []
                            const Comp = form2Comp(type);
                            return type === "description" ?
                                <View className={classNames('lxf_info_text')}>
                                    {title}
                                </View>
                                : <LxfCard title={`${title}${required ? '*' : ''}`} >
                                    {Comp ?
                                        <Comp
                                            mode={type}
                                            name={name}
                                            options={options}
                                            placeholder={`请输入${title}`}
                                        />
                                        : null}
                                </LxfCard>
                        })}

                        <View className={`${baseCls}-submit-btn`}>
                            <LxfButton title='提交' formType='submit' />
                        </View>
                    </Form>
                </View>
            </View>
            <LxfConfirmModal ref={signRef} title={"确定提交问答文字的内容？"} showCancelBtn showOkBtn okText={"提交"} onOk={handleConfirm} />
            <LxfConfirmModal ref={passRef} title={<><View>恭喜你通过考试</View><View>请耐心等待审核结果</View></>} showOkBtn okText={"我知道了"} componentProps={{ content: `你的分数为${step2Res?.score}分，高于${step2Res?.pass_score}分` }} onOk={() => goToPage('/pages/signup/step3/index')} />
            <LxfConfirmModal ref={cannotPassRef} title={'分数不足，未能通过考试'} showOkBtn okText={<LxfButton style={{ marginBottom: 20, width: 'calc(100% - 40px)' }} title='继续答题' />} componentProps={{ content: `抱歉，您的分数为${step2Res?.score}分，低于${step2Res?.pass_score}份，请重新答题提交！` }} />
        </View>
    )
}
export default withUser({
    auto: true,
    inject: true
})(SignUp)
