import { LxfInput, LxfPicker, LxfRadioGroup, LxfTextarea } from "@/components"
import { IFromType } from "@/models/apply"
import CityComp from "./components/city"
import DateComp from "./components/date"
import UniversityComp from "./components/school"

export const form2Comp = (type: IFromType) => {
  const map = {
    "city": CityComp,
    "select": LxfRadioGroup,
    "textarea": LxfTextarea,
    "text": LxfInput,
    "radio": LxfRadioGroup,
    "date": DateComp,
    "search:university": UniversityComp,
  }
  return map[type] || null
}