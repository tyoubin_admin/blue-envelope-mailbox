import './index.scss'

import classNames from 'classnames'
import { View, Image } from '@tarojs/components'
import IconEmojisad from '@/assets/icons/emojisad.svg';
import { LxfButton } from '@/components';
import { goToPage } from '@/utils';

const baseCls = 'disuse';

const Disuse = () => {

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <Image className={`${baseCls}-wrapper-icon`} src={IconEmojisad} />
                <View className={classNames('lxf_h3_text_2')}>感谢你对乡村儿童笔友项目的支持 <br />很遗憾你未通过通信大使的审核</View>
                <LxfButton className={`${baseCls}-wrapper-btn`} title='回到首页' onClick={() => goToPage('/pages/welcome/index')} />
            </View>
        </View>
    )
}
export default Disuse;
