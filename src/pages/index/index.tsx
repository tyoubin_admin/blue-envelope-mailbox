import './index.scss'

import { useEffect, useState } from 'react'
import { View } from '@tarojs/components'

import { EApplicatStatus, EVolunteerStatus } from '@/constants/volunteer'
import { useUser, withUser } from '@/hooks'
import { goToPage } from '@/utils'
import { LxfPanel } from '@/components'
import Taro from '@tarojs/taro'
import { STORAGE_GENERAL_INFO_KEY } from '@/constants'

const baseCls = 'signup-page';

const Index = () => {
    const [{ userInfo }] = useUser();
    const [step, setStep] = useState<number>();
    const storageGeneral = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
    useEffect(() => {
        if (!userInfo) {
            setStep(1)
            return;
        }
        const status = userInfo?.status;
        if (status === EVolunteerStatus.DISUSE) {
            goToPage('/pages/disuse/index');
        } else if (status === EVolunteerStatus.NORMAL) {
            goToPage('/pages/mailbox/index')
        }
        // 0,1_2跳第一阶段申请;
        // 1_1,2_2跳第二阶段申请;
        // 2_1,3_2跳第三阶段申请;
        // 1_0,2_0,3_0跳申请审核中
        const applicat_status = userInfo?.applicat_status;
        if ([EApplicatStatus.STEP_1_IN_APPLICATION, EApplicatStatus.STEP_2_IN_APPLICATION, EApplicatStatus.STEP_3_IN_APPLICATION].includes(applicat_status)) {
            goToPage('/pages/in-application/index');
        } else if (applicat_status === EApplicatStatus.STEP_3_PASSED) {
            goToPage('/pages/mailbox/index');
        } else if ([EApplicatStatus.IN_APPLICATION, EApplicatStatus.STEP_1_RETURN].includes(applicat_status)) {
            setStep(1);
        } else if ([EApplicatStatus.STEP_1_PASSED, EApplicatStatus.STEP_2_RETURN].includes(applicat_status)) {
            setStep(2);
        } else if ([EApplicatStatus.STEP_2_PASSED, EApplicatStatus.STEP_3_RETURN].includes(applicat_status)) {
            setStep(3);
        }

    }, [userInfo])

    if (!step) {
        return null;
    }

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <View className={"lxf_h1_text"}>欢迎报名蓝信封通信大使</View>
                <View className={`${baseCls}-info`}>
                    <View className={'lxf_info_text'}>蓝信封【乡村儿童的书信笔友】项目，通过志愿者与乡村儿童一对一写信，以朋辈陪伴方式鼓励孩子在书信空间表达情感，引导其健康快乐成长！</View>
                    <View className={`${baseCls}-info-desp`}>
                        <LxfPanel title={'报名时间'} content={'全年开放报名'} type={'info'} />
                        <LxfPanel title={'报名要求'} content={'18周岁及以上（或已获高校录取通知书）'} type={'info'} />
                    </View>
                    <View className={`${baseCls}-info-action`}>
                        <View className={"lxf_h3_text"}>志愿者申请流程</View>
                        <LxfPanel title={'一、个人信息注册'} content={'填写个人基本信息与自我介绍'} buttonText={'去注册'} onClick={() => goToPage('/pages/signup/step1/index')} disabled={step !== 1} />
                        <LxfPanel title={'二、观看书信培训视频'} content={'认识通信规范，收发信运作等信息'} buttonText={'去观看'} onClick={() => goToPage('/pages/signup/step2/video/index')} disabled={step !== 2 || storageGeneral?.videoVisited} />
                        <LxfPanel title={'三、理论考试'} content={'通过试题评定你是否能成为通信大使'} buttonText={'去考试'} onClick={() => goToPage(step === 3 ? '/pages/signup/step3/index' : '/pages/signup/step2/exam/index')} disabled={![2, 3].includes(step)} />
                        <View className={'lxf_info_text'}>说明：从完成考试，到通知考核结果，到配对孩子，到获得孩子第一封信，总时长为30-60天，请耐心等待。</View>
                    </View>
                </View>
            </View>
        </View>
    )
}
export default withUser({
    auto: true,
    inject: true
})(Index)
