import './index.scss'

import React, { useEffect } from 'react'
import { Image, Swiper, SwiperItem, Text, View } from '@tarojs/components'
import { goToPage } from '@/utils'
import { LxfButton } from '@/components'
import { STORAGE_GENERAL_INFO_KEY } from '@/constants'
import Taro from '@tarojs/taro'
import { useUser } from '@/hooks'
import { EVolunteerStatus } from '@/constants/volunteer'

const baseCls = 'index-page';

const Welcome = () => {
  const imageList = [
    'https://tt642f1292fb1394a001-env-as0wwifwof.tos-cn-beijing.volces.com/welcome1.png',
    'https://tt642f1292fb1394a001-env-as0wwifwof.tos-cn-beijing.volces.com/welcome2.png'
  ]
  useEffect(() => {
    const data = Taro.getStorageSync(STORAGE_GENERAL_INFO_KEY) ?? {};
    if (!data?.joined) {
      Taro.setStorageSync(STORAGE_GENERAL_INFO_KEY, {...data, joined: true });
    } else {
      const [{ userInfo }] = useUser();
      if (userInfo?.status !== EVolunteerStatus.DISUSE) {
        goToPage('/pages/index/index');
      }
    }
  }, [])

  return (
    <View className={baseCls}>
      <View className={`${baseCls}-wrapper`}>
        <Swiper
          className={`${baseCls}-swiper`}
          indicatorActiveColor={'rgba(22, 24, 35, 0.15)'}
          indicatorColor={'#F3F3F4'}
          adjustHeight={'highest'}
          circular
          indicatorDots
          autoplay>
          {imageList.map(image =>
            <SwiperItem className={`${baseCls}-swiper-card`}>
              <Image src={image} />
            </SwiperItem>)}
        </Swiper>
        <View className={`${baseCls}-footer`}>
          <LxfButton className={`${baseCls}-signbtn`} title={'报名成为蓝信封通信大使'} onClick={() => goToPage('/pages/index/index')} />
          <Text className={`${baseCls}-signdesp`}>已有超过十万志愿者加入蓝信封</Text>
        </View>
      </View>
    </View>
  )
}
export default Welcome;
