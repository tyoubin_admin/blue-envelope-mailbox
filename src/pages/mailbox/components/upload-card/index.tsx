import './index.scss'

import Taro from '@tarojs/taro'
import { Image, View } from '@tarojs/components'

import api from '@/api'
import IconCamera from '@/assets/icons/camera.svg'

const baseCls = 'upload-card';

const UploadCard = (props) => {
    const { onUpload } = props;
    const chooseImage = () => {
        Taro.chooseImage({
            count: 1, // 默认9
            sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有，在H5浏览器端支持使用 `user` 和 `environment`分别指定为前后摄像头
            success: async (res) => {
                // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
                const tempFilePaths = res.tempFilePaths;
                const ossRes = await api.lxf.uploadOss({ file: tempFilePaths[0] });
                onUpload?.(ossRes);
            }
        })
    }
    return (<>
        <View className={`${baseCls}`} onClick={chooseImage}>
            <View className={`${baseCls}-back`} />
            <Image className={`${baseCls}-icon`} src={IconCamera} />
        </View>
    </>)

}

export default UploadCard;