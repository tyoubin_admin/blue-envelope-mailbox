import './index.scss'

import Taro from '@tarojs/taro'
import { Icon, Image, View } from '@tarojs/components'

import { IUploadOssRes } from '@/service/type'

const baseCls = 'letter-card';

interface ILetterCard {
    letterInfo: IUploadOssRes;
    id?: number;
    onDelete: (id: number) => void;
}

const LetterCard = (props: ILetterCard) => {
    const { letterInfo, id, onDelete } = props;
    return <View className={`${baseCls}`} >
        <Icon className={`${baseCls}-delete`} size='15' type='cancel' color='white' onClick={() => onDelete(letterInfo.file_id)} />
        <View className={`${baseCls}-back`} />
        <Image className={`${baseCls}-letter`} src={letterInfo.file_url} onClick={() => {
            Taro.previewImage({
                current: letterInfo.file_url, // 当前显示图片的http链接
                urls: [letterInfo.file_url] // 需要预览的图片http链接列表
            })
        }} />
        {id ? <View className={`${baseCls}-id`} >{id}</View> : null}
    </View>
}

export default LetterCard;