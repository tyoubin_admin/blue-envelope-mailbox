import './index.scss'

import dayjs from 'dayjs'
import React, { useMemo, useRef } from 'react'
import { Image, ScrollView, Text, View } from '@tarojs/components'

import IconArrowRight from '@/assets/icons/arrow-right.svg'
import IconWrite from '@/assets/icons/write.svg'
import { LxfEnvelope } from '@/components'
import { useCommunication, withCommunication } from '@/hooks'
import { ELetterSender, ELetterStatus, ICommunicationLetterItem } from '@/models/communication'
import { goToPage } from '@/utils'
import { IFloatWrapperRef } from '../login'
import { ChildInfoDrawer } from './components/child-info-drawer'

const baseCls = 'mailbox-page';

const MailBox = () => {
  const childInfoRef = useRef<IFloatWrapperRef>(null);
  const [{ communicationInfo, communicationLetterInfo }] = useCommunication();
  const letter_list = communicationLetterInfo?.letter_list ?? [];
  const object = communicationInfo ? `通信对象：${communicationInfo?.child_name ?? '获取失败'}` : '请耐心等待匹配结果';
  const title = useMemo(() => {
    if (!communicationInfo) {
      return '匹配通信对象中...';
    }
    if (!letter_list.length) {
      return '完成匹配，请耐心等待来信。';
    }
    const sendCnt = letter_list.filter(letter => letter.sender === ELetterSender.Volunteer).length;
    if (sendCnt > 0) {
      return `已寄出${sendCnt}封信`
    }
    return '尚未寄出回信'
  }, [letter_list, communicationInfo])

  return (
    <View className={baseCls}>
      <View className={`${baseCls}-wrapper`}>
        <View className={`${baseCls}-wrapper-scroll`}>
          <View className={`${baseCls}-top`} onClick={() => {
            childInfoRef.current?.open()
          }}>
            <View className={`${baseCls}-title`} >
              {title}
            </View>
            <View className={`${baseCls}-object`} onClick={() => {
              childInfoRef.current?.open()
            }}>
              <Text className={`${baseCls}-object-text`}> {object} </Text>
              {communicationInfo && <Image className={`${baseCls}-icon-arrow-right`} src={IconArrowRight} mode='aspectFit' />}
            </View>
          </View>
          <View className={`envelope-list`}>
            {letter_list.length ? letter_list.map((letter: ICommunicationLetterItem) => {
              const { status, sender, create_date, url } = letter ?? {};
              return <LxfEnvelope status={status} title={sender === ELetterSender.Child ? `${communicationInfo?.child_name}的来信` : '我的回信'} date={dayjs(create_date * 1000).format('YYYY.MM.DD')} url={url} />
            }) : <LxfEnvelope status={ELetterStatus.Waiting} title={'截至目前，尚未收到来信'} date={dayjs().format('YYYY.MM.DD')} />}
          </View>
        </View >
        <View className={`${baseCls}-write`} onClick={() => goToPage('/pages/mailbox/write/index')}>
          <Image src={IconWrite} />
        </View>
      </View>
      <ChildInfoDrawer
        ref={childInfoRef}
        maskClosable
        height={525}
        title={communicationInfo?.child_name}
        showBack
        hideTitle
        style={{ borderRadius: '26px 26px 0 0' }}
      />
    </View>
  )
}

export default withCommunication({
  auto: true,
  inject: true
})(MailBox)
