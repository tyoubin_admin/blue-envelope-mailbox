import React, { FunctionComponent } from 'react';
import { Image, Input, View } from '@tarojs/components';
import { useUser, withUser } from '@/hooks';
import InfoItem from '../components/info-item/info-item';

import './index.scss';

const baseCls = 'mobile-modify';

const Certificate: FunctionComponent<{}> = props => {

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                
            </View>
        </View>
    );
};

export default withUser({
    auto: true,
    inject: false,
})(Certificate)
