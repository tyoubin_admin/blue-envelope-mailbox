import './index.scss'

import classNames from 'classnames'
import { View, Image } from '@tarojs/components'
import IconInApplition from '@/assets/icons/heartshield.svg';
import { LxfButton } from '@/components';

const baseCls = 'in-application';

const InApplition = () => {

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <Image className={`${baseCls}-wrapper-icon`} src={IconInApplition} />
                <View className={classNames('lxf_h2_text_2')}>你已成功提交考核的申请资料</View>
                <View className={classNames('lxf_h3_text_2')}>项目工作人员正在进行审核，并将在 21 个工作日 内予以申请结果通知，请耐心等待。</View>
                <LxfButton className={`${baseCls}-wrapper-share-btn`} title='让更多人来参与' openType='share' />
            </View>
        </View>
    )
}
export default InApplition;
