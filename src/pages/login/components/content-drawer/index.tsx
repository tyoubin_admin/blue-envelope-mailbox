import './index.scss'

import { FunctionComponent } from 'react'
import { Text, View } from '@tarojs/components'

import { LxfFloatWrapper } from '@/components'

const baseCls = 'login-agreement';

const LoginAgreement: FunctionComponent<{}> = () => {
  return (
    <View className={baseCls}>
      <View className={`${baseCls}-row`}>
        <Text>
          获取你的昵称、头像、性别及地区
        </Text>
      </View>
    </View>
  );
};

export default LoginAgreement;

const defaultProps = {
  title: '蓝信封',
};

export const LoginContentDrawer = LxfFloatWrapper.withDrawer<{}>({
  defaultProps,
})(LoginAgreement);
