import './index.scss'

import React, { FunctionComponent } from 'react'
import { Image, Text, View } from '@tarojs/components'

import IconArrowRight from '@/assets/icons/arrow-right-2.svg'
import { useUser, withUser, useCommunication, withCommunication } from '@/hooks'
import { goToPage } from '@/utils'
import dayjs from 'dayjs'

const baseCls = 'profile-page';
const userCls = `${baseCls}-user`;

const SelectItem = props => {
  const { title, onClick } = props;
  return (
    <View className={`${userCls}-select-item`} onClick={onClick}>
      <Text className={`${userCls}-select-item-title`}>{title}</Text>
      <Image className={`${userCls}-select-item-icon`} src={IconArrowRight}></Image>
    </View>)
}
const Profile: FunctionComponent<{}> = props => {
  const [{ userInfo }] = useUser();
  const [{ communicationInfo }] = useCommunication();
  const { name, create_date } = userInfo ?? {};
  const [year, month, day] = create_date?.split(' ')[0]?.split('-') ?? []
  const { letter_v_count } = communicationInfo ?? {};
  const desc = [`${year}年${month}月${day}日成为志愿者`, `已寄出${letter_v_count}封信`]

  return (
    <View className={baseCls}>
      <View className={`${baseCls}-wrapper`}>
        <View className={`${userCls}-info`}>
          <View className={`${userCls}-info-avatar`}>
            <Image className={`${userCls}-info-avatar-img`} src={userInfo?.avatar_url || ''} />
          </View>
          <View className={`${userCls}-info-right`}>
            <View className={`${userCls}-info-name`}>
              {name}（已加入 {dayjs().diff(dayjs(create_date), 'day')} 天）
            </View>
            {desc.map(text => <View className={`${userCls}-info-desc`}>
              {text}
            </View>)}
          </View>
        </View>
        <View className={`${userCls}-select`} >
          <SelectItem title={'个人资料'} onClick={() => goToPage('/pages/my/person/index')} />
          <SelectItem title={'志愿者证书'} onClick={() => goToPage('/pages/certificate/index')} />
          <SelectItem title={'求助咨询'} onClick={""} />
        </View>
      </View>
    </View>
  );
};

export default withUser({
  auto: true,
  inject: false,
})(withCommunication({
  auto: true,
  inject: false,
})(Profile))
