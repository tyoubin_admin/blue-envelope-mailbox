import './index.scss'

import React, { FunctionComponent, useState } from 'react'
import { Input, View } from '@tarojs/components'

import api from '@/api'
import { withUser } from '@/hooks'
import { message } from '@/utils'
import { LxfButton } from '@/components'

import InfoItem from '../components/info-item/info-item'

const baseCls = 'mobile-modify';

const MobileModify: FunctionComponent<{}> = props => {
    const [phoneNumber, setPhoneNumber] = useState<string>();

    const onSave = async () => {
       await api.lxf.updateMobileNumber({ phoneNumber });
       message.success('修改成功');
    }

    return (
        <View className={baseCls}>
            <View className={`${baseCls}-wrapper`}>
                <InfoItem title={'手机'} content={
                    <Input
                        type='text'
                        placeholder='输入你的手机号'
                        focus
                        style={{ textAlign: 'left', color: 'rgba(22, 24, 35, 0.60)' }}
                        onInput={e => {
                            setPhoneNumber(e.target.value)
                        }} />} />
                <View className={`${baseCls}-savebtn`}>
                    <LxfButton title={'保存'} disabled={!phoneNumber} onClick={onSave} />
                </View>
            </View>
        </View>
    );
};

export default withUser({
    auto: true,
    inject: false,
})(MobileModify)
